from setuptools import setup

setup(
    name='voronoi',
    version='0.7',
    packages=['lib'],
    scripts=['bin/run_voronoi_code.py'],
    data_files=[('voronoi_example', ['data/test.csv'])],
    requires=['matplotlib', 'numpy', 'pandas', 'scipy', 'vorbin', 'astropy', 'shapely', 'descartes'],
    url='',
    license='',
    author='Sebastian Kamann',
    author_email='s.kamann@ljmu.ac.uk',
    description='Code to create Voronoi maps of mean velocity and dispersion from radial velocity data.'
)
