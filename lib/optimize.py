import logging
import numpy as np
import emcee
from scipy.optimize import minimize


logger = logging.getLogger(__name__)


class MaxLike(object):
    """
    This class is designed to determine the mean velocity and the velocity
    dispersion from a set of radial velocities and uncertainties.

    More generally, the class may also be used to obtain an estimate of the
    intrinsic mean and scatter of a data set with uncertainties.

    Note that this code was strongly influenced bt the ideas presented here:
    http://dan.iel.fm/emcee/current/user/line/
    """

    # parameters = ['v0', 'v_disp']
    LABELS = {'v0': r'$v_{\rm 0}\,[{\rm km\,s^{-1}}]$', 'v_disp': r'$\sigma_{\rm LOS}\,[{\rm km\,s^{-1}}]$',
              'theta0': r'$\theta_{\rm 0}$', 'v_rot': r'$v_{\rm rot}\,[{\rm km\,s^{-1}}]$'}

    def __init__(self, v, v_err, fix_v0=None):
        """
        Initialize a new instance of the MaxLike class.

        Parameters
        ----------
        v : array_like
            The measured velocities of the sample stars.
        v_err : array_like
            The uncertainties of the measured velocities.
        fix_v0 : float, optional
             In case a fixed mean velocity should be used, it can be provided
             here.
        """
        self.v = np.asarray(v)
        self.v_err = np.asarray(v_err)

        # Obtain reasonable values for the initial guesses of the model parameters
        if fix_v0 is None:
            self.v0 = np.nanmedian(self.v)
            self.v0_fixed = False
        else:
            self.v0 = fix_v0
            self.v0_fixed = True

        self.v_disp = np.nanstd(self.v)

        self.corner = None

    @property
    def data(self):
        """
        Returns a list containing the arrays with the input data.
        """
        return [self.v, self.v_err]

    @property
    def parameters(self):
        """
        Returns the parameters of the model.
        """
        return ['v0', 'v_disp'] if not self.v0_fixed else ['v_disp', ]

    @property
    def initial_guess(self):
        """
        Returns a list containing the initial guesses for the model
        parameters.
        """
        return [self.v0, self.v_disp] if not self.v0_fixed else [self.v_disp, ]

    @property
    def n_parameters(self):
        """
        Returns the number of parameters that are determined.
        """
        return len(self.parameters)

    @staticmethod
    def check_results(results):
        """
        This method checks if the values obtained for the model parameters
        are valid. It should be overwritten by classes that inherit from
        MaxLike.

        Parameters
        ----------
        results : array_like
            The values obtained for the model parameters.

        Returns
        -------
        results : array_like
            The corrected values for the model parameters.
        """
        # dispersion must be positive
        if results[-1] < 0:
            results[-1] *= -1

        return results

    @staticmethod
    def analyse_mc_chains(samples):
        """
        This method obtains estimates for the median values and the upper and
        lower limits of the uncertainty interval of each model parameter.

        This is done by first determining the 16%, 50%, and 86% percentiles
        from the distributions returned by the MCMC chains. The uncertainties
        are then estimated as p[86%] - p[50%]  and p[50%] - p[16%]

        Parameters
        ----------
        samples : ndarray
            The distributions returned by the MCMC chains.

        Returns
        -------
        results : list
            A list containing an iterator over the median value, the upper,
            and the lower limit of each model parameter.
        """

        mapper = map(lambda p: (p[1], p[2] - p[1], p[1] - p[0]),
                     zip(*np.percentile(samples, [16, 50, 84], axis=0)))
        return [result for result in mapper]

    # @staticmethod
    def ln_prior(self, init_guess):
        """
        Check if the priors for the model parameters are fulfilled.

        This method implements the priors needed for the MCMC estimation
        of the uncertainties. Uninformative priors are used, i.e. the
        likelihoods are constant across the accepted value range and zero
        otherwise.

        Parameters
        ----------
        init_guess : array_like
            The current values of the model parameters.

        Returns
        -------
        loglike : float
            The log likelihood of the model for the given parameters. As
            uninformative priors are used, the log likelihood will be zero
            for valid parameters and -inf otherwise.
        """
        if not self.v0_fixed:
            v0, v_disp = init_guess
        else:
            v0 = self.v0
            v_disp = init_guess[0]
        if -1000 < v0 < 1000 and v_disp > 0:
            return 0
        else:
            return -np.inf

    # @staticmethod
    def ln_like(self, init_guess, data):
        """
        Calculate the log likelihood of the current model given the data.

        It is assumed that the distribution follows a Gaussian distribution.
        Therefore, the probability p of a single measurement (v, v_err) is
        estimated as:

        p = exp{-(v - v0)**2/[2*(v_disp^2 + v_err^2)]}/[2.*(v_disp^2 + v_err^2)]

        Then the log likelihood is then determined by summing over the
        probabilities of all measurements and taking the ln: loglike = ln(sum(p))

        Parameters
        ----------
        init_guess : array_like
            The current values of the model parameters.
        data : array_like
            The measured data and associated uncertainties.

        Returns
        -------
        loglike : float
            The log likelihood of the data given the current model.
        """
        if len(init_guess) == 2:  # note that super-class may provide theta-dependent v0 even when it is fixed.
            v0, v_disp = init_guess
        else:
            v0 = self.v0
            v_disp = init_guess[0]
        v, v_err = data

        norm = v_err * v_err + v_disp * v_disp
        sum1 = np.sum(np.log(norm))
        sum2 = np.sum(np.power(v - v0, 2) / norm)

        return -0.5 * sum1 - 0.5 * sum2

    # @classmethod
    def ln_prob(self, init_guess, data):
        """
        This function calculates the log likelihood of the data given a model.
        In contrast to the method MaxLike.ln_like, it also implements the
        priors on the model parameters.

        Parameters
        ----------
        init_guess : array_like
            The current values of the model parameters.
        data : array_like
            The measured data and associated uncertainties.

        Returns
        -------
        loglike : float
            The log likelihood of the data given the current model taking into'
            account the priors on the model parameters.
        """
        lp = self.ln_prior(init_guess)
        if not np.isfinite(lp):
            return -np.inf
        return self.ln_like(init_guess, data) + lp

    def __call__(self, mc=False, n_walkers=100, burn=100, plot=False, return_chains=False, **kwargs):
        """
        Determine the intrinsic parameters of the velocity distribution.

        Parameters
        ----------
        mc : bool, optional
            Flag indicating if the uncertainties of each parameter should be
            determined via an MCMC analysis.
        n_walkers : int, optional
            The number of walkers used for the MCMC analysis.
        burn : int, optional
            Number of steps ignored at the start of each chain when getting
            results from chains.
        plot : bool, optional
            Flag indicating if a corner plot showing the results from the MCMC
            analysis should be displayed.
        return_chains : bool, optional
            Flag indicating if the individual MCMC chains should be returned
            with the results.
        kwargs
            Additional keyword arguments may be used to provide initial guesses
            for the parameters of the velocity distribution.

        Returns
        -------
        best_fit : OptimizeResult
           The optimization result represented as a OptimizeResult object.
           Important attributes are: x the solution array, success a Boolean
           flag indicating if the optimizer exited successfully and message
           which describes the cause of the termination. See
           scipy.optimize.OptimizeResult for a description of other attributes.
           Note that if the mc flag is set, the results from the MCMC analysis
           will be stored as attribute mc. Also, attribute parameters contains
           the parameter names.
        """
        self.v0 = kwargs.pop('v0', self.v0)
        self.v_disp = kwargs.pop('v_disp', self.v_disp)
        if kwargs:
            raise IOError('Unknown keyword argument(s) provided: {0}'.format(kwargs))

        nll = lambda *args: -1.*self.ln_like(*args)
        best_fit = minimize(nll, self.initial_guess, args=(self.data,), method='Nelder-Mead')
        # check if results fulfil priors
        best_fit['x'] = self.check_results(best_fit['x'])

        if mc:
            pos = [best_fit['x'] + np.random.randn(self.n_parameters) for _ in range(n_walkers)]
            # check if starting values fulfil priors
            for i in range(len(pos)):
                pos[i] = self.check_results(pos[i])

            sampler = emcee.EnsembleSampler(n_walkers, self.n_parameters, self.ln_prob, args=(self.data,))
            sampler.run_mcmc(pos, 500)

            # import matplotlib.pyplot as plt
            # for i in range(n_walkers):
            #     plt.plot(sampler.chain[i, :, -1], 'g-')  # dispersion chains
            #     # plt.plot(sampler.chain[i, :, 1], 'r-')
            # plt.show()

            samples = sampler.chain[:, burn:, :].reshape((-1, self.n_parameters))
            if 'theta0' in self.parameters:
                # make sure endpoints of all chains are in range [-PI, PI) for theta0
                i = self.parameters.index('theta0')
                samples[:, i] = np.mod(samples[:, i] + np.pi, 2.*np.pi) - np.pi

            if return_chains:
                best_fit['chains'] = sampler.chain

            if plot:
                import corner
                import matplotlib.pyplot as plt
                self.corner = corner.corner(samples, labels=[self.LABELS[p] for p in self.parameters],
                                            truths=best_fit['x'])
                ndim = len(self.parameters)
                axes = np.array(self.corner.axes).reshape((ndim, ndim))
                for yi in range(1, ndim):
                    ax = axes[yi, 0]
                    ax.set_ylabel(ax.get_ylabel(), fontsize=20)
                    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
                        label.set_fontsize(16)
                for xi in range(ndim):
                    ax = axes[-1, xi]
                    ax.set_xlabel(ax.get_xlabel(), fontsize=20)
                    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
                        label.set_fontsize(16)
                plt.show()

            # add MCMC results to OptimizeResult instance
            best_fit['mc'] = self.analyse_mc_chains(samples=samples)

        # add parameter names to OptimizeResult instance
        best_fit['parameters'] = self.parameters

        return best_fit


class RotatingMaxLike(MaxLike):
    """
    This class expands the application range of the MaxLike class to models
    that include rotation with an amplitude v_rot around an axis theta0.
    """

    def __init__(self, theta, v, v_err, fix_v0=None):

        self.theta = theta
        self.theta0 = np.pi
        self.v_rot = 1.
        super(RotatingMaxLike, self).__init__(v=v, v_err=v_err, fix_v0=fix_v0)

    @property
    def data(self):
        data = super(RotatingMaxLike, self).data
        data.insert(0, self.theta)
        return data

    @property
    def parameters(self):
        return ['theta0', 'v_rot'] + super(RotatingMaxLike, self).parameters

    @property
    def initial_guess(self):
        return [self.theta0, self.v_rot] + super(RotatingMaxLike, self).initial_guess

    @staticmethod
    def check_results(results):
        if results[1] < 0:
            results[1] *= -1
            results[0] += np.pi
        if not -np.pi <= results[0] < np.pi:
            results[0] = np.mod(results[0] + np.pi, 2*np.pi) - np.pi
        return super(RotatingMaxLike, RotatingMaxLike).check_results(results)

    # The following routines were inspired by http://dan.iel.fm/emcee/current/user/line/
    # @staticmethod
    def ln_prior(self, init_guess):
        theta0, v_rot = init_guess[:2]
        value = super().ln_prior(init_guess[2:])

        # IMPORTANT: constraining theta0 to [-PI, PI) does not work because if theta ~ PI, then chains with initial
        # values ~ -PI will get stuck at the lower limit. Hence the probabilities are tapered off proportional to
        # SQRT(2*PI - abs(theta0)/PI) for absolute values of theta0 between PI and 2xPI
        if v_rot > 0:
            if -np.pi <= theta0 < np.pi:
                return value
            elif abs(theta0) < 2.*np.pi:
                return 0.5*np.log10((2.*np.pi - abs(theta0))/np.pi) + value
            else:
                return -np.inf
        else:
            return -np.inf

    # @staticmethod
    def ln_like(self, init_guess, data):
        if not self.v0_fixed:
            theta0, v_rot, v0 = init_guess[:3]
        else:
            theta0, v_rot = init_guess[:2]
            v0 = self.v0
        theta = data[0]

        # change mean velocity so that it includes rotation
        v0 = v0 + v_rot*np.sin(theta - theta0)
        return super().ln_like([v0, ] + list(init_guess[-1:]), data[1:])

    @staticmethod
    def analyse_mc_chains(samples):

        # make sure theta is not at the edge of the valid angle range
        # see https://en.wikipedia.org/wiki/Mean_of_circular_quantities
        theta0 = np.arctan2(np.sin(samples[:, 0]).sum(), np.cos(samples[:, 0]).sum())

        samples[:, 0] = np.mod(samples[:, 0] - theta0 + np.pi, 2.*np.pi) - np.pi
        results = super(RotatingMaxLike, RotatingMaxLike).analyse_mc_chains(samples=samples)

        # undo theta0 shift
        results[0] = (results[0][0] + theta0, results[0][1], results[0][2])

        return results

    def __call__(self, **kwargs):

        self.theta0 = kwargs.pop('theta0', self.theta0)
        self.v_rot = kwargs.pop('v_rot', self.v_rot)

        return super(RotatingMaxLike, self).__call__(**kwargs)
