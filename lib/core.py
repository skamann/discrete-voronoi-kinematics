import logging
import numpy as np
from astropy.table import QTable

from .binning import make_voronoi_bins
from .optimize import MaxLike
from .plot import plot_kinematic_voronoi_maps


logger = logging.getLogger(__name__)


def ml_estimate_for_sample(v, verr, kappa=None, mc=False, fix_v0=None):
    """
    Estimate the kinematic properties using a subsample of the available
    data.

    Parameters
    ----------
    v
    verr
    kappa : float, optional
        The efficiency of the kappa-sigma clipping (if requested). The
        default behaviour is to do no clipping.
    mc : bool, optional
        If this flag is set, the uncertainties in each bin will be
        determined by an MCMC analysis.
    fix_v0 : float, optional
        In case the mean velocity should be fixed to a given value, it
        must be provided here. Default is to optimize the mean velocity as
        well.

    Returns
    -------
    results : OptimizeResult
        The optimization result represented as a OptimizeResult object.
        See gcdb.util.optimize.dynamics.momentsml for a complete
        description.
    """
    logger.info('Obtaining ML estimate for sample of {0} stars.'.format(len(v)))

    excluded = np.zeros(len(v), dtype=np.bool)

    results = None
    for n in range(4):

        ml_estimator = MaxLike(v=v[~excluded], v_err=verr[~excluded], fix_v0=fix_v0)

        results = ml_estimator(mc=mc, plot=False)
        if kappa:
            v_model = results['x'][0] if fix_v0 is None else fix_v0
            v_disp = results['x'][-1]

            excluded = (v - v_model) * (v - v_model) > kappa * kappa * (v_disp ** 2 + verr ** 2)

            logger.info(' clipped {0} star(s)'.format(excluded.sum()))
            if not excluded.any():
                break
        else:
            break

    return results


def create_voronoi_map(data, pixelsize, nstars=50, kappa=None, mc=False, outfilename=None, plot=False,
                       plotfilename=None, display='both', pointings=None, gridfilename=None, vlims_rot=None,
                       vlims_disp=None, patches_rot=None, patches_disp=None, stars=False):

    x_bin, y_bin, bins = make_voronoi_bins(x=data['X'].value, y=data['Y'].value, pixelsize=pixelsize.value,
                                           target_sn=np.sqrt(nstars), gridfilename=gridfilename)

    final = []
    for i in range(bins.max() + 1):

        include = (bins == i)
        logger.info('Current bin: #{0}/{1}'.format(i + 1, bins.max()))
        logger.info(' N_stars = {0}'.format(include.size))
        logger.info(' median velocity error = {0:.1f}.'.format(np.median(data['STAR V err'][include])))
        logger.info(' max. velocity error = {0:.1f}.'.format(np.max(data['STAR V err'][include])))

        data_i = {'x bin': x_bin[i], 'y bin': y_bin[i]}

        results_i = ml_estimate_for_sample(v=data['STAR V'][include], verr=data['STAR V err'][include],
                                           kappa=kappa, mc=mc)
        # Kinematics._log_results(results=results_i, mc=mc)
        for k, parameter in enumerate(results_i['parameters']):
            data_i[parameter] = results_i['x'][k]
            if mc:
                median, high, low = results_i['mc'][k]
                data_i['{0} mc low'.format(parameter)] = low
                data_i['{0} mc median'.format(parameter)] = median
                data_i['{0} mc high'.format(parameter)] = high

        final.append(data_i)

    results = QTable(final)
    print(results)
    if outfilename is not None:
        results.write(outfilename, format='ascii.ecsv')

    if plot:
        if display in ['mean', 'both']:
            mean = results['v0'] if not mc else results['v0 mc median']
        else:
            mean = None
        if display in ['dispersion', 'both']:
            dispersion = results['v_disp'] if not mc else results['v_disp mc median']
        else:
            dispersion = None

        if stars:
            original_data = data[['X', 'Y']].values.T
        else:
            original_data = None

        plot_kinematic_voronoi_maps(mean, dispersion, results['x bin'], results['y bin'], plotfilename=plotfilename,
                                    pointings=pointings, vlims_rot=vlims_rot, vlims_disp=vlims_disp,
                                    patches_rot=patches_rot, patches_disp=patches_disp, original_data=original_data)

    return results
