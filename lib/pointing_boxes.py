import logging
import numpy as np
from matplotlib import patches, path
from astropy import units as u
from astropy.nddata import NDData


logger = logging.getLogger(__name__)


# The dimensions of the MUSE field of view, given are the distances of the 4 edges to the centre of the FoV in degrees
MUSE_FOV = NDData(np.array(
    [[0.0086683878, 0.0084439565],
     [-0.00872, 0.008499508],
     [-0.00828, -0.0082782215],
     [0.0080579666, -0.0083893106],
     [0.0086683878, 0.0084439565]]), unit=u.deg)


MUSE_FOV_NFM = NDData(np.array(
    [[0.001111, 0.001111],
     [-0.001111, 0.001111],
     [-0.001111, -0.001111],
     [0.001111, -0.001111],
     [0.001111, 0.001111]]), unit=u.deg)


class PointingBoxes(object):

    def __init__(self, pointings, fov=None):

        if fov is None:
            fov = {}
        fov['wfm'] = MUSE_FOV
        fov['nfm'] = MUSE_FOV_NFM

        self.paths = []

        # draw them
        for pointing in pointings:

            x = u.Quantity(pointing['x'])
            if x.unit == u.dimensionless_unscaled:
                x *= u.deg
            y = u.Quantity(pointing['y'])
            if y.unit == u.dimensionless_unscaled:
                y *= u.deg
            pa = u.Quantity(pointing['pa'])
            if pa.unit == u.dimensionless_unscaled:
                pa = u.deg

            pointing_edges = fov['wfm']
            if 'fov' in pointings.columns:
                if pointing['fov'] in fov.keys():
                    pointing_edges = fov[pointing['fov']]
                else:
                    logger.error("Unknown field ov view provided: '{0}'".format(pointing['fov']))

            # calculate offsets, taking into account position angle of field of view
            offsets_xy = NDData(np.dot(pointing_edges.data,
                                       np.array([[np.cos(pa), np.sin(pa)], [-np.sin(pa), np.cos(pa)]])),
                                unit=pointing_edges.unit)

            # get edges of current pointing
            edges = np.zeros(offsets_xy.data.shape, dtype=np.float32)
            for i in range(offsets_xy.data.shape[0]):
                # add offset to pointing position
                dx = x + offsets_xy.data[i, 0]*offsets_xy.unit
                dy = y + offsets_xy.data[i, 1]*offsets_xy.unit
                edges[i] = dx.value, dy.value

            # add path
            self.paths.append(path.Path(edges, closed=True))

    @property
    def patches(self):
        return [patches.PathPatch(musepath) for musepath in self.paths]

    def draw_patches(self, ax, color='red', linewidth=1, linestyle='--'):

        for musepath in self.paths:
            musepatch = patches.PathPatch(musepath, facecolor="None", edgecolor=color,
                                          lw=linewidth, ls=linestyle, alpha=1.0)
            ax.add_patch(musepatch)

    def find_union(self):
        from shapely.geometry import Polygon
        from shapely.ops import cascaded_union

        polygons = []
        for musepath in self.paths:
            vertices = musepath.vertices
            x = vertices[:, 0]
            y = vertices[:, 1]
            polygons.append(Polygon([(i[0], i[1]) for i in zip(x, y)]))

        return cascaded_union(polygons)
