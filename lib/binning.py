import logging
import numpy as np
import pandas as pd
from matplotlib.patches import Polygon
from scipy.spatial import KDTree, Voronoi
from vorbin.voronoi_2d_binning import voronoi_2d_binning


logger = logging.getLogger(__name__)


# function taken from http://stackoverflow.com/questions/20515554/colorize-voronoi-diagram
def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite Voronoi regions in a 2D diagram to finite regions.

    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.

    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.
    """
    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge
            t = vor.points[p2] - vor.points[p1]  # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:, 1] - c[1], vs[:, 0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())

    return new_regions, np.asarray(new_vertices)


def _find_polygons(x_bin, y_bin):
    v = Voronoi(np.vstack((x_bin, y_bin)).T)
    regions, vertices = voronoi_finite_polygons_2d(v)

    polygons = []
    for i, region in enumerate(regions):
        # if -1 not in region and len(region) > 0:
        polygon = Polygon(vertices[region], True)
        polygons.append(polygon)
    return polygons


def make_voronoi_bins(x, y, pixelsize, target_sn, weights=None, noise='poissonian', gridfilename=None):

    # create grid with requested pixel size, make sure that [0,0] (=cluster centre) is one grid point.
    x_min, x_max = x.min() - np.mod(x.min(), pixelsize), x.max()
    y_min, y_max = y.min() - np.mod(y.min(), pixelsize), y.max()
    _grid = np.mgrid[x_min:x_max:pixelsize, y_min:y_max:pixelsize]
    grid = _grid.reshape((2, -1)).T

    # if requested, store limits of super-pixel grid in csv-file:
    if gridfilename is not None:
        out = pd.Series({'pixelsize': pixelsize, 'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max})
        out.to_csv(gridfilename)

    # find nearest grid point for every data point
    gridpoints = KDTree(grid)
    _, nearest = gridpoints.query(np.vstack((x, y)).T)

    # count stars in each grid cell, 'minlength' makes sure last bins are included even if empty
    stars_in_bin = np.bincount(nearest, minlength=gridpoints.data.shape[0], weights=weights)

    # calculate "S/N"
    hasdata = (stars_in_bin > 0)
    signal = stars_in_bin[hasdata]

    # calculate noise depending on noise list_option
    if noise == 'constant':
        noise = np.ones(len(signal))
    elif noise == 'poissonian':
        noise = np.sqrt(signal)
    else:
        raise IOError('Unsupported value for option "noise": {0}'.format(noise))

    # carry out Voronoi tesselation
    try:
        results = voronoi_2d_binning(grid[hasdata, 0], grid[hasdata, 1], signal, noise, target_sn,
                                     pixelsize=None, quiet=True, plot=False)
    except ValueError as msg:
        raise ValueError(msg)

    bin_number, x_bin, y_bin = results[:3]
    polygons = _find_polygons(x_bin, y_bin)

    in_polygon = -1 * np.ones(len(x), dtype=np.bool)
    # for i, (x, y) in enumerate(zip(self.x, self.y)):
    for j, polygon in enumerate(polygons):
        in_polygon[polygon.get_path().contains_points(list(zip(x, y)))] = j

    bin_number = np.asarray(in_polygon, dtype='<i4')
    return x_bin, y_bin, bin_number
