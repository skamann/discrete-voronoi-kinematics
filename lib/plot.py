import logging
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors, colorbar
from matplotlib.collections import PatchCollection
from scipy.stats import sigmaclip

from .binning import _find_polygons

logger = logging.getLogger(__name__)


def make_voronoi_plot(values, x, y, patches=True, vmin=None, vmax=None, cmap=plt.cm.seismic, axes=None, axes_cbar=None,
                      cbar_orientation='vertical', center=None, pointings_mask=None, cb_ticks=None):

    if not axes:
        fig = plt.figure()
        axes = fig.add_axes([0.07, 0.06, 0.9, 0.9])
        axes.minorticks_on()
    else:
        fig = None

    # get colour map
    if not vmin:
        vmin = sigmaclip(values, 2, 2)[0].min()
    if not vmax:
        vmax = sigmaclip(values, 2, 2)[0].max()
    norm = colors.Normalize(vmin=vmin, vmax=vmax)

    # draw the patches or single stars?
    if patches:
        polygons = _find_polygons(x, y)

        if pointings_mask is not None:
            from descartes.patch import PolygonPatch
            from shapely.geometry import MultiPolygon, Polygon

            # check if a mask from pointings in provided. If so, it must be given as a
            # shapely MultiPolynomial object. We do a loop over the polynomials, create
            # a patch from it and use the patch borders to clip the patches of the Voronoi
            # bins.
            if not isinstance(pointings_mask, MultiPolygon):
                pointings_mask = MultiPolygon([pointings_mask, Polygon()])
            for pointing_polygon in pointings_mask:
                pointing_patch = PolygonPatch(pointing_polygon, linewidth=1.5, edgecolor='k', facecolor='None')
                axes.add_patch(pointing_patch)

                # a new PatchCollection must be created for each polygon because otherwise the clip_path will
                # be overwritten each time a new polygon is used.
                p = PatchCollection(polygons, cmap=cmap, norm=norm)
                p.set_array(np.asarray(values))
                p.set_clip_path(pointing_patch)
                cax = axes.add_collection(p)
        else:
            # just create collection of patches and draw them
            p = PatchCollection(polygons, cmap=cmap, norm=norm)
            p.set_array(np.asarray(values))
            cax = axes.add_collection(p)
    else:
        # draw single stars
        cax = axes.scatter(x, y, marker='o', cmap=cmap, norm=norm, linewidths=0, c=values)

    # set equal aspect ratio
    axes.set_aspect('equal')

    # set limits of graph
    if pointings_mask is None or pointings_mask.is_empty:
        axes.set_xlim(x.max(), x.min())
        axes.set_ylim(y.min(), y.max())
    else:
        (xmin, ymin, xmax, ymax) = pointings_mask.bounds
        axes.set_xlim(xmax, xmin)
        axes.set_ylim(ymin, ymax)

    # format ticks
    # axes.xaxis.set_major_formatter(self.x_formatter)
    # axes.yaxis.set_major_formatter(self.y_formatter)

    # add colorbar
    if axes_cbar is None:
        cbar = axes.get_figure().colorbar(cax, cmap=cmap, shrink=0.8, orientation=cbar_orientation, ticks=cb_ticks)
    else:
        cbar = colorbar.ColorbarBase(axes_cbar, cmap=cmap, norm=norm, orientation=cbar_orientation, ticks=cb_ticks)
    cbar.ax.set_label('Values')

    # draw central star
    if center:
        x_center, y_center = center
        axes.plot(x_center, y_center, ls="None", marker=(5, 1, 0), mew=1.0, mec="k", ms=16, mfc='r')

    if fig is not None:
        plt.show()

    # # set tick formater and labels
    # if self.options.value('rel2centre'):
    #     # set default formatter
    #     self.ax.xaxis.set_major_formatter(ScalarFormatter())
    #     self.ax.yaxis.set_major_formatter(ScalarFormatter())
    #
    #     # set labels
    #     self.ax.set_xlabel('RA - Cluster centre [arcsec]')
    #     self.ax.set_ylabel('Dec - Cluster centre [arcsec]')
    #
    # else:
    #     # format tick as degrees
    #     def fmt_ra(tick, val):
    #         return astCoords.decimal2hms(tick / 3600., ':')
    #
    #     def fmt_dec(tick, val):
    #         return astCoords.decimal2dms(tick / 3600., ':')
    #
    #     # set it
    #     self.ax.xaxis.set_major_formatter(FuncFormatter(fmt_ra))
    #     self.ax.yaxis.set_major_formatter(FuncFormatter(fmt_dec))
    #
    #     # set labels
    #     self.ax.set_xlabel('RA')
    #     self.ax.set_ylabel('Dec')

    return cbar


def plot_kinematic_voronoi_maps(mean, dispersion, x, y, cmap_rot=plt.cm.RdBu_r, cmap_disp=plt.cm.gnuplot,
                                plotfilename=None, pointings=None, vlims_rot=None, vlims_disp=None, patches_rot=None,
                                patches_disp=None, original_data=None):

    # plot only one panel
    if mean is None or dispersion is None:
        fig = plt.figure(figsize=(8, 7))
        ax = fig.add_axes([0.12, 0.12, 0.725, 0.83])
        ax.minorticks_on()
        ax_cbar = fig.add_axes([0.86, 0.12, 0.02, 0.83])

        ax_rot = ax if dispersion is None else None
        ax_cbar_rot = ax_cbar if dispersion is None else None

        ax_disp = ax if mean is None else None
        ax_cbar_disp = ax_cbar if mean is None else None

        axes = [ax, ]
    else:
        fig = plt.figure(figsize=(16, 7))

        ax_rot = fig.add_axes([0.06, 0.12, 0.36, 0.83])
        ax_rot.minorticks_on()
        ax_cbar_rot = fig.add_axes([0.425, 0.12, 0.01, 0.83])

        ax_disp = fig.add_axes([0.56, 0.12, 0.36, 0.83])
        ax_disp.minorticks_on()
        ax_cbar_disp = fig.add_axes([0.925, 0.12, 0.01, 0.83])

        axes = [ax_rot, ax_disp]

    # add colour-coded Voronoi bins to plot(s)
    if mean is not None:
        if vlims_rot is not None:
            assert len(vlims_rot) == 2, 'If provided, "vlims_rot" must be tuple of length 2.'
            vmin, vmax = vlims_rot
        else:
            vmin, vmax = None, None

        cb = make_voronoi_plot(mean, x, y, cmap=cmap_rot, axes=ax_rot, axes_cbar=ax_cbar_rot, pointings_mask=pointings,
                               vmin=vmin, vmax=vmax)
        cb.set_label(r"$\bar v\ [\mathrm{km/s}]$", fontsize=18)
        ax_cbar_rot.tick_params(axis='y', which='both', width=1.5)

        if patches_rot is not None:
            for patch in patches_rot:
                ax_rot.add_patch(patch)

        if original_data is not None:
            x_input, y_input = original_data
            ax_rot.scatter(x_input, y_input, marker='o', c='k', s=7)

    if dispersion is not None:
        if vlims_disp is not None:
            assert len(vlims_disp) == 2, 'If provided, "vlims_disp" must be tuple of length 2.'
            vmin, vmax = vlims_disp
        else:
            vmin, vmax = None, None

        cb = make_voronoi_plot(dispersion, x, y, cmap=cmap_disp, axes=ax_disp, axes_cbar=ax_cbar_disp,
                               pointings_mask=pointings, vmin=vmin, vmax=vmax)
        cb.set_label(r"$\sigma_\mathrm{v}\ [\mathrm{km/s}]$", fontsize=18)
        ax_cbar_disp.tick_params(axis='y', which='both', width=1.5)

        if patches_disp is not None:
            for patch in patches_disp:
                ax_disp.add_patch(patch)

        if original_data is not None:
            x_input, y_input = original_data
            ax_disp.scatter(x_input, y_input, marker='o', c='k', s=7)

    if mean is not None and dispersion is not None:
        ax_rot.text(0.05, 0.95, r"$\bar v$", fontsize=36, va="top", transform=ax_rot.transAxes)
        ax_rot.text(-0.15, 1.03, "$(a)$", fontsize=32, va="top", transform=ax_rot.transAxes)
        ax_disp.text(0.05, 0.95, r"$\sigma_\mathrm{v}$", fontsize=36, va="top", transform=ax_disp.transAxes)
        ax_disp.text(-0.15, 1.03, "$(b)$", fontsize=32, va="top", transform=ax_disp.transAxes)

    for ax in axes:
        ax.plot(0., 0., ls="None", marker="+", mew=3.0, mec="w", ms=16)
        ax.tick_params(axis="both", which="both", width=1.5)

        ax.set_xlabel(r"$\Delta\mathrm{RA}\,[\mathrm{arcmin}]$", fontsize=18)
        ax.set_ylabel(r"$\Delta\mathrm{Dec}\,[\mathrm{arcmin}]$", fontsize=18)

    if plotfilename is not None:
        fig.savefig(plotfilename)
    else:
        plt.show()
    return fig
