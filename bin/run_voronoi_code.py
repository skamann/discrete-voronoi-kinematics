#! /usr/bin/env python
import argparse
import logging
import numpy as np
from matplotlib.patches import Ellipse, Polygon
from astropy import units as u
from astropy.table import QTable

from lib.core import create_voronoi_map
from lib.pointing_boxes import PointingBoxes

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument('data', type=str, help='Csv-file containing coordinates and velocities.')
parser.add_argument('-p', '--pixelsize', type=u.Quantity, help='Pixel size used to pre-bin the data.')
parser.add_argument('-n', '--nstars', type=int, default=100, help='Target number of stars per Voronoi bin.')
parser.add_argument('-w', '--window', type=u.Quantity, nargs=4, help='Rectangular window in which to create maps.')
parser.add_argument('--outfilename', type=str, help='Name of file used to store Voronoi data in csv-format.')
parser.add_argument('--plotfilename', type=str, help='Name of file used to store final plot.')
parser.add_argument('--vlims_rot',  type=float, nargs=2, help='Min./max. cuts used to show rotation map.')
parser.add_argument('--vlims_disp', type=float, nargs=2, help='Min./max. cuts used to show dispersion map.')
parser.add_argument('--angle', type=u.Quantity, help='Position angle (ccw in deg.) to highlight in rotation map.')
parser.add_argument('--ellipse', type=u.Quantity, nargs=3,
                    help='semi-major axis, eccentricity, and position angle (ccw in deg.) of ellipse to overplot.')
parser.add_argument('--stars', action='store_true', default=False, help='Show locations of stars on Voronoi maps.')
parser.add_argument('--mc', action='store_true', default=False,
                    help='Use Markov-chain Monte Carlo approach to infer uncertainties?')
parser.add_argument('--xycolumns', type=str, nargs=2, default=['X', 'Y'],
                    help='Names of columns containing x- and y-offsets to cluster centre.')
parser.add_argument('--vradcolumns', type=str, nargs=2, default=['STAR V', 'STAR V ERR'],
                    help='Names of columns containing velocity measurements and uncertainties.')
parser.add_argument('--pointings', type=str, help='Name of file containing centres and position angles of pointings.')

args = parser.parse_args()

# data = Table.read(args.data)
data = QTable.read(args.data)

for column in args.xycolumns + args.vradcolumns:
    if column not in data.columns:
        logger.error('Column "{0}" missing in provided data.'.format(column))
        exit(1)

data.rename_column(args.xycolumns[0], 'X')
data.rename_column(args.xycolumns[1], 'Y')
data.rename_column(args.vradcolumns[0], 'STAR V')
data.rename_column(args.vradcolumns[1], 'STAR V err')

if data['X'].unit in [None, u.dimensionless_unscaled] or data['Y'].unit in [None, u.dimensionless_unscaled]:
    data['X'].unit = u.arcmin
    data['Y'].unit = u.arcmin
    logger.warning('Missing coordinates units in input data. Assuming {0}.'.format(data['X'].unit))
if data['STAR V'].unit in [None, u.dimensionless_unscaled] or data['STAR V err'].unit in [None, u.dimensionless_unscaled]:
    data['STAR V'].unit = u.km/u.s
    data['STAR V err'].unit = u.km/u.s
    logger.warning('Missing velocities units in input data. Assuming {0}.'.format(data['STAR V'].unit))

if args.window is not None:
    for i, entry in enumerate(args.window):
        if entry.unit in [None, u.dimensionless_unscaled]:
            args.window[i] = u.Quantity(entry.value, unit=data['X'].unit)
    xmin, xmax, ymin, ymax = args.window
    slc = (data['X'] > xmin) & (data['X'] < xmax) & (data['Y'] > ymin) & (data['Y'] < ymax)
    data = data[slc]

if args.pixelsize is None:
    args.pixelsize = (data['X'].max() - data['X'].min())/40.
elif args.pixelsize.unit in [None, u.dimensionless_unscaled]:
    args.pixelsize = u.Quantity(args.pixelsize.value, unit=data['X'].unit)

patches_rot = []
patches_disp = []

if args.angle is not None:
    if args.angle.unit in [None, u.dimensionless_unscaled]:
        args.angle = u.Quantity(args.angle.value, unit=u.deg)

    theta = np.deg2rad(90.*u.deg - args.angle)
    dx = 5.*args.pixelsize.to(data['X'].unit)*np.cos(theta)
    dy = 5.*args.pixelsize.to(data['X'].unit)*np.sin(theta)
    patches_rot.append(Polygon(xy=[[-dx.value, -dy.value], [dx.value, dy.value]], linewidth=1.5, edgecolor='g'))

if args.ellipse is not None:
    rh, e, _theta = args.ellipse

    if rh.unit in [None, u.dimensionless_unscaled]:
        rh = u.Quantity(rh.value, unit=data['X'].unit)

    if _theta.unit in [None, u.dimensionless_unscaled]:
        _theta = u.Quantity(_theta.value, unit=u.deg)

    # make sure semi-major axis angle is measured north through east
    theta = 90.*u.deg - _theta

    patches_rot.append(Ellipse((0, 0), width=rh.to(data['X@'].unit).value,
                               height=rh.to(data['X@'].unit).value * np.sqrt(1. - e ** 2), angle=theta,
                               facecolor='None', linewidth=1.5, edgecolor='g'))
    patches_disp.append(Ellipse((0, 0), width=rh.to(data['X@'].unit).value,
                                height=rh.to(data['X@'].unit).value * np.sqrt(1. - e ** 2), angle=theta,
                                facecolor='None', linewidth=1.5, edgecolor='g'))

if args.pointings is not None:
    pointings_list = QTable.read(args.pointings, format='ascii.ecsv')
    pboxes = PointingBoxes(pointings_list)

    pointings = pboxes.find_union()
else:
    pointings = None

results = create_voronoi_map(data=data, pixelsize=args.pixelsize, nstars=args.nstars, mc=args.mc, plot=True,
                             outfilename=args.outfilename, plotfilename=args.plotfilename, vlims_rot=args.vlims_rot,
                             vlims_disp=args.vlims_disp, patches_rot=patches_rot, patches_disp=patches_disp,
                             stars=args.stars, pointings=pointings)
